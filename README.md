# README #

Questo repository contiene tutto il codice per trainare una rete neurale con diversi paremetri modificabili runtime
ed eventualmente caricarlo su un microcontrollore. 

## Disclaimer ##

Tutte le librerie Python relative al machine learning e alla computer vision cambiano nelle loro interfacce ed implementazioni di continuo ed in modo non retrocompatibile.

Tutto il software quindi è testato per funzionare con le versioni specifiche indicate nel file requirements.txt.

Utilizzare versioni più recenti delle varie librerie ha garanzia quasi certa di non funzionare, ove possibile ho cercato di usare le versioni più recenti ma stabili dei pacchetti.


## Come eseguire ##

Installarsi un ambiente virtuale sotto virtualenv, aggiornare pip ed installarsi i requisiti con i seguenti comandi:


**Linux**:
```
cd sound_deep_learning/  
virtualenv --python=/usr/bin/python3.7 venv
source venv/bin/activate  
pip install -r requirements.txt  
```


### Keyword spotting for Microcontrollers ###

Per trainare una rete DNN con 3 strati fully-connected con 144 neuroni ogni strato, lanciare: 

```
python2.7 train.py --model_architecture dnn --model_size_info 144 144 144 --dct_coefficient_count 10 --window_size_ms 40 --window_stride_ms 40 --learning_rate 0.0005,0.0001,0.00002 --how_many_training_steps 10000,10000,10000 --summaries_dir work/DNN/DNN1/retrain_logs --train_dir work/DNN/DNN1/training 
```

Si può cambiare il modello della rete tra quelli disponibili (dnn, ds_cnn) cambiando 
da command line l'argomento *--model_architecture* . Inoltre l'argomento *--model_size_info*
viene utilizzato per passare alla rete neurale diversi parametri come il numero di layers,
convolution filter e così via. Per genereare diversi modelli, guardare nel train_commands.txt. 

Se non si specificano nella command line, le dimensioni di default di *--window_size_ms* e 
*--window_stride_ms* sono quelle in fondo al train.py file. Eventualmente si possono cambiare
in run time. 

Lanciando il comando di train si scarica in automatico il dataset contenente tutti i file
con cui si effettua il training della rete (pesa circa 2GB). 

In input-data.py è presente la gestione di questi file,che vengono divisi in percentuale 80-10-10
tra train-validation-test set: in particolare i file prodotti dallo stesso utente verranno messi 
nello stesso set, attraverso l'utilizzo del "_nohash_" presente nel nome del file. 

In models.py sono presenti le funzioni che vengono chiamate per creare i diversi modelli delle 
reti neurali.

Gli altri file (freeze.py, test.py, quant_models.py) vengono utilizzati per runnare interference sul
modello trainato da un determinato checkpoint. Rifersi a https://github.com/ARM-software/ML-KWS-for-MCU